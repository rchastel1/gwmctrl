# GWMCtrl

## Description

A GNOME Shell Window Manager controller. As stated in the description of [wmctrl](https://www.freedesktop.org/wiki/Software/wmctrl/) from which this tool is inspired, it aims to provide a

> command line access to almost all the features defined in the EWMH specification. It can be used, for example, to obtain information about the window manager, to get a detailed list of desktops and managed windows, to switch and resize desktops, to make windows full-screen, always-above or sticky, and to activate, close, move, resize, maximize and minimize them. The command line access to these window management functions makes it easy to automate and execute them from any application that is able to run a command in response to an event.

## Architecture

This project ships two components to interact with the GNOME Shell Window Manager ([Mutter](https://mutter.gnome.org/)) : 

* A DBus service embedded in a GNOME extension that serves as a backend to query and command Mutter,
* A command line frontend `gwmctrl` that empowers the Linux console with the ability to maximize, minimize, move windows etc ...

## Installation

```shell
git clone https://gitlab.com/rchastel1/gwmctrl.git
make install
```

### Requirements

#### GNOME Shell compatibility

Tested on GNOME Shell 45.

The current code base is incompatible with version prior to 45 mostly because of the newly introduced import syntax. Backports to earlier versions are welcome.

#### GNU Guile

The gwmctrl command line tool is written in the GNU Guile Scheme dialect. 
Tested on guile 3.0.9.

#### $PATH adjustment

The command line tool `gwmctrl` will be installed in `~/.local/bin`, make sure this directory is contained in your `$PATH` variable. If not, add 

```shell
export PATH="$PATH:$HOME/.local/bin"
```

to your `.bashrc`.

## Usage

### Listing window properties
```shell
gwmctrl list window-properties
```

### Listing windows

#### General syntax
```shell
gwmctrl list windows [--select | -s [<field-name><select-operator><field-value>]+]*
```

- `<select-operator>` must be one of : `= != < <= > >= ~`.
- `~` denotes partial string matching.

#### Listing all windows
```shell
gwmctrl list windows
```

#### Showing the currently focused window:
```shell
gwmctrl list windows --select focus='<true>'
```

#### Listing the minimized windows of the third workspace:
```shell
gwmctrl list windows -s minimized='<true>' workspace='<int64 3>'
```

#### Listing the minimized windows of the first 3 workspaces and those that are not horizontally maximized
```shell
gwmctrl list windows -s 'minimized=<true>' 'workspace<=<int64 3>' \
                     -s 'maximized.h!=<true>'
```

### Setting window properties

#### General syntax
```shell
gwmctrl set windows [--select | -s selection+]+ -- [<field-name>=<field-value>]+
```

#### Minimizing the focused window:
```shell
gwmctrl set windows -s focus='<true>' -- minimized='<true>'
```

#### Raising a particular window and sticking it on all workspace:
```shell
gwmctrl set windows -s id='<int64 12345>' -- focus='<true>' sticked='<true>
```

## Roadmap

- [x] Listing windows
- [x] Setting window properties
- [x] Proper handling of failures
- [x] Filtering windows
- [x] Moving windows
- [x] Resizing windows
- [x] Listing window properties
- [ ] Listing workspaces
- [ ] Listing workspace properties
- [ ] Managing workspaces
- [ ] Listing monitors
- [ ] Listing monitor properties
- [ ] Managing monitors
- [ ] JSON / XML input/output
- [ ] Proper packaging

## Author & Contributors

RChastel (https://gitlab.com/rchastel1)

## License

GPL-2.0-or-later (see https://gitlab.com/rchastel1/gwmctrl#license)

## Project status

Early development/Highly unstable.
