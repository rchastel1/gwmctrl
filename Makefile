all: install

.PHONY: install lint

install:
	install -d ~/.local/share/gnome-shell/extensions
	cp -a gwmctrl@rchastel.dev/ ~/.local/share/gnome-shell/extensions/
	install -/.local/bin
	cp --preserve=mode gwmctrl ~/.local/bin

lint:
	eslint gwmctrl@rchastel.dev
