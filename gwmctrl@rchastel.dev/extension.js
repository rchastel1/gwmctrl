/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import Shell from 'gi://Shell';
import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';

import Window from './window.js';

export default class GWMCtrl {

    static dbus_interface_spec =
        '<node>' +
        '  <interface name="dev.rchastel.gwmctrl">';

    static {
        GWMCtrl.dbus_interface_spec +=
            '    <method name="listWindows">' +
            '      <arg type="aa(ssv)" direction="in" name="query" />' +
            '      <arg type="aa{sv}" direction="out" name="window_list" />' +
            '    </method>'; 
    }
    /**
     * listWindows:
     *
     * List windows matching a selection
     * 
     * @param {aa(ssv)} selection: filter declarations used to narrow the set
     * of windows to return.
     *
     * Selection follows the following grammar :
     *
     * <selection>    ::= [<or-clause>, ...]
     * <or-clause>    ::= [<and-clause>, <and-clause> ...]
     * <and-clause>   ::= (<cmp-operator>, <field-name>, field-value>)
     * <cmp-operator> ::= "=" | "!=" | "<" | "<=" | ">" | ">=" | "~"
     * <field-name> ::= a window field name
     * <field-value> ::= a window field value
     *
     * <or-clause> are joined with logical OR,
     * <and-clause> are joined with logical AND,
     * and each <cmp-operator> denotes a predicates to be used for matching
     * window field named field-name with the given field-value value.
     *
     * Example:
     * [[("=", "id", <int64 12345>), ("!=", "minimized", <true>)],
     *  [("<", "workspace", <int64 3>)]]
     * will match windows that have ((id === 1234 && minimized !== true) ||
     *                               (workspace < 3))
     * 
     * @returns {aa{sv}} the list of windows managed by the window manager
     * filtered with selection.
     */
    listWindows(selection) {
        return GWMCtrl.runQuery(
            global.display.list_all_windows().map(Window.newWindow),
            selection,
            Window.fields,
            true
        ).map(w => w.pack());
    }

    static {
        GWMCtrl.dbus_interface_spec +=
            '    <method name="listWindowProperties">' +
            '      <arg type="a{sa{sv}}" direction="out" ' +
            '           name="window_properties" />' +
            '    </method>'; 
    }
    /**
     * listWindowProperties:
     *
     * List window properties
     * 
     * @returns {a{sa{sv}}} a dictionary that describes all available window
     * properties.
     *
     * Each property entry is a mapping organized as follows :
     *
     * 'property_name': {
     *   'type': <dbus_type_string>,
     *   'mutable': <true> if the property is mutable, <false> otherwise,
     *   <sub-property> ...
     * }
     */
    listWindowProperties() {
        const rec = (input, output, reserved_properties, depth) => {
            for(const [property_name, property] of Object.entries(input)) {
                if (!reserved_properties.includes(property_name)) {
                    output[property_name] = {
                        type: GLib.Variant.new_string(property.type.repr),
                        mutable:
                            GLib.Variant.new_boolean(property.set != undefined),
                    };
                    rec(property, output[property_name], reserved_properties,
                        depth + 1);
                    if (depth > 0) {
                        output[property_name] =
                            GLib.Variant.new('a{sv}', output[property_name]);
                    }
                }
            }
            return output;
        };
        return rec(Window.fields, {},
                   Window.fields.reserved_properties, 0);
    }

    static {
        GWMCtrl.dbus_interface_spec +=
            '    <method name="setWindowProperties">' +
            '      <arg type="aa(ssv)" direction="in" name="selection" />' +
            '      <arg type="a(sv)" direction="in" name="properties" />' +
            '      <arg type="b" direction="out" name="success?" />' +
            '    </method>';
    }
    /**
     * setWindowProperties:
     *
     * Sets a bunch of properties on a specific set of windows.
     *
     * @param {aa(ssv)} selection: filter declarations used to narrow the set
     * of windows to return. @see listWindows, for a complete description of
     * the selection grammar
     * @param {a(sv)} properties: the properties to set
     * @returns {b} true if the operation succeed, false if selection
     * leads to an empty set of windows
     * @throws {PropertyLookupError} if some property do not match any window
     * property
     * @throws {ReadOnlyPropertyError} if some property are read-only
     *
     */
    setWindowProperties(selection, properties) {

        const windows = GWMCtrl.runQuery(
            global.display.list_all_windows().map(Window.newWindow),
            selection,
            Window.fields
        );

        if (windows.length == 0) return false;

        for(const [property_path, g_value] of properties) {

            const prop_spec = GWMCtrl.lookupProperty(
                Window.fields,
                property_path,
                Window.fields.reserved_properties
            );

            if (!('set' in prop_spec))
                throw new GWMCtrl.ReadOnlyPropertyError(property_path);

            const value = prop_spec.unpack(g_value);
            
            for(const window of windows){
                prop_spec.set(window.getBackend(), value);
            }
        }

        return true;
    }
    
    static PropertyLookupError = class extends Error {
        constructor(property) {
            super("Cannot find property " + property);
        }
    }

    static ReadOnlyPropertyError = class extends Error {
        constructor(property) {
            super("Property " + property + " is read only");
        }
    }

    static lookupProperty(obj, property_path, reserved_properties) {
        let property = obj;
        for(const path_elt of property_path.split('.')) {
            if(reserved_properties.includes(path_elt) || property == undefined)
                throw new GWMCtrl.PropertyLookupError(property_path);
            property = property[path_elt];
        }

        if (property == undefined || property === obj)
            throw new GWMCtrl.PropertyLookupError(property_path);
        return property;
    }

    static makeBasicCheckFactories(predicates) {
        let factories = {};
        for (const [type, predicate] of Object.entries(predicates)) {
            factories[type] = (property_path, value, unpacker) => {
                return obj => {
                    const obj_property =
                          GWMCtrl.lookupProperty(obj,
                                                 property_path,
                                                 unpacker.reserved_properties);
                    const property_unpacker =
                          GWMCtrl.lookupProperty(unpacker,
                                                 property_path,
                                                 unpacker.reserved_properties);
                    return predicate(obj_property,
                                     property_unpacker.unpack(value));
                };
            }; 
        }
        return factories;
    }

    static basic_checks = GWMCtrl.makeBasicCheckFactories({
        '=': (obj_value, query_value) => obj_value === query_value,
        '!=': (obj_value, query_value) => obj_value !== query_value,
        '>': (obj_value, query_value) => obj_value > query_value,
        '>=': (obj_value, query_value) => obj_value >= query_value,
        '<': (obj_value, query_value) => obj_value < query_value,
        '<=': (obj_value, query_value) => obj_value <= query_value,
        '~': (obj_value, query_value) => {throw new Error('Not Implemented');},
    });

    static OperatorNotFoundError = class extends Error {
        constructor(operator) {
            super('Cannot find operator' + operator);
        }
    }

    static makeOrCheck(checks) {
        return obj => checks.some(check => check(obj));
    }

    static makeAndCheck(checks) {
        return obj => checks.every(check => check(obj));
    }

    static runQuery(hits, check_specs, unpacker, empty_means_all = false) {
        if (check_specs.length == 0) {
            if (empty_means_all) return hits;
            return [];
        }
        return hits.filter(GWMCtrl.makeOrCheck(
            check_specs.map(alt_spec => {
                return GWMCtrl.makeAndCheck(alt_spec.map(check_params => {
                    const [check_type, property, value] = check_params;
                    const check_factory = GWMCtrl.basic_checks[check_type];
                    if(check_factory == undefined)
                        throw new GWMCtrl.OperatorNotFoundError(check_type);
                    return check_factory(property, value, unpacker);
                }));
            })
        ));
    }

    enable() {
        this.__dbus_interface =
            Gio.DBusExportedObject
            .wrapJSObject(GWMCtrl.dbus_interface_spec, this);
        this.__dbus_interface.export(Gio.DBus.session, '/dev/rchastel/gwmctrl');
    }

    disable() {
        this.__dbus_interface?.flush();
        this.__dbus_interface?.unexport();
        delete this.__dbus_interface;
    }

    static {
        GWMCtrl.dbus_interface_spec +=
            '  </interface>' +
            '</node>';
    }
}

