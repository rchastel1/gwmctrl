/* window.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import Meta from 'gi://Meta';
import * as GType from './gtype.js';

export default class Window {

    static window_types = [
        "normal",
        "desktop",
        "dock",
        "dialog",
        "modal_dialog",
        "toolbar",
        "menu",
        "utility",
        "splashscreen",
        "dropdown_menu",
        "popup_menu",
        "tooltip",
        "notification",
        "combobox",
        "drag_and_drop",
        "override_other"
    ];

    static fields = {

        reserved_properties: ['type', 'get', 'set', 'pack', 'unpack',
                              'reserved_properties'],

        id: new GType.Property({
            type: GType.Int,
            get: window => window.get_id()
        }),
        class: new GType.Property({
            type: GType.Str,
            get: window => window.wm_class
        }),
        type: new GType.Property({
            type: GType.Str,
            get: window => Window.window_types[window.window_type]
        }),
        workspace: new GType.Property({
            type: GType.Int,
            get: window => window.get_workspace().workspace_index
        }),
        pid: new GType.Property({
            type: GType.Int,
            get: window => window.get_pid()
        }),
        title: new GType.Property({
            type: GType.Str,
            get: window => window.get_title()
        }),
        rect: new GType.Property({
            type: GType.Dict('a{sx}', {x: GType.Int, y: GType.Int,
                                       width: GType.Int, height: GType.Int}),
            get: window => {
                const window_rect = window.get_frame_rect();
                return {
                    x: window_rect.x,
                    y: window_rect.y,
                    width: window_rect.width,
                    height: window_rect.height
                };
            },
            set: (window, value) => {
                Window.windowRunUnmaximizeIfNeeded(window, (w) => {
                    w.move_resize_frame(true, value.x, value.y,
                                        value.width, value.height);
                });
            },
            x: new GType.Property({
                type: GType.Int,
                get: (window) => window.get_frame_rect().x,
                set: (window, value) => {
                    const rect = window.get_frame_rect();
                    Window.windowRunUnmaximizeIfNeeded(window, (w) => {
                       w.move_resize_frame(true, value, rect.y,
                                           rect.width, rect.height);
                    });
                }
            }),
            y: new GType.Property({
                type: GType.Int,
                get: (window) => window.get_frame_rect().y,
                set: (window, value) => {
                    const rect = window.get_frame_rect();
                    Window.windowRunUnmaximizeIfNeeded(window, (w) => {
                       w.move_resize_frame(true, rect.x, value,
                                           rect.width, rect.height);
                    });
                }
            }),
            width: new GType.Property({
                type: GType.Int,
                get: (window) => window.get_frame_rect().width,
                set: (window, value) => {
                    const rect = window.get_frame_rect();
                    Window.windowRunUnmaximizeIfNeeded(window, (w) => {
                        w.move_resize_frame(true, rect.x, rect.y,
                                            value, rect.height);
                    });
                }
            }),
            height: new GType.Property({
                type: GType.Int,
                get: (window) => window.get_frame_rect().height,
                set: (window, value) => {
                    const rect = window.get_frame_rect();
                    Window.windowRunUnmaximizeIfNeeded(window, (w) => {
                        w.move_resize_frame(true, rect.x, rect.y,
                                            rect.width, value);
                    });
                }
            })
        }),
        focus: new GType.Property({
            type: GType.Bool,
            get: window => window.has_focus(),
            set: (window, value) => {if (value) window.activate(0); }
        }),
        minimized: new GType.Property({
            type: GType.Bool,
            get: window => window.minimized,
            set: (window, value) => value ? window.minimize() : window.unminize()
        }),
        maximized: new GType.Property({
            type: GType.Dict('a{sb}', {h: GType.Bool, v: GType.Bool}),
            get: window => {
                return {
                    h: window.maximized_horizontally,
                    v: window.maximized_vertically
                };
            },
            set: (window, value) => {
                const dispatch = (h) => {
                    if (h) return (v) => {
                        if(v) window.maximize(Meta.MaximizeFlags.BOTH);
                        else {
                            const sig = window.connect('size-changed', (w) => {
                                window.disconnect(sig);
                                window.maximize(Meta.MaximizeFlags.HORIZONTAL);
                            });
                            window.unmaximize(Meta.MaximizeFlags.VERTICAL);
                        }
                    };
                    else return (v) => {
                        if(v) {
                            const sig = window.connect('size-changed', (w) => {
                                window.disconnect(sig);
                                window.maximize(Meta.MaximizeFlags.VERTICAL);
                            });
                            window.unmaximize(Meta.MaximizeFlags.HORIZONTAL);
                        }
                        else window.unmaximize(Meta.MaximizeFlags.BOTH);
                    };
                };
                dispatch(value.h)(value.v);
            },
            h: new GType.Property({
                type: GType.Bool,
                get: window => window.maximized_horizontally,
                set: (window, value) => {
                    if(value) window.maximize(Meta.MaximizeFlags.HORIZONTAL);
                    else window.unmaximize(Meta.MaximizeFlags.HORIZONTAL);
                }
            }),
            v: new GType.Property({
                type: GType.Bool,
                get: window => window.maximized_vertically,
                set: (window, value) => {
                    if(value) window.maximize(Meta.MaximizeFlags.VERTICAL);
                    else window.unmaximize(Meta.MaximizeFlags.VERTICAL);
                }
            })
        }),
        fullscreen: new GType.Property({
            type: GType.Bool,
            get: window => window.is_fullscreen(),
            set: (window, value) => {
                value ? window.make_fullscreen() : window.unmake_fullscreen();
            }
        }),
        hidden: new GType.Property({
            type: GType.Bool,
            get: window => window.is_hidden()
        }),
        floating: new GType.Property({
            type: GType.Bool,
            get: window => window.is_floating()
        }),
        above: new GType.Property({
            type: GType.Bool,
            get: window => window.above,
            set: (window, value) => {
                value ? window.make_above() : window.unmake_above();
            }
        }),
        sticked: new GType.Property({
            type: GType.Bool,
            get: window => window.on_all_workspaces,
            set: (window, value) => value ? window.stick() : window.unstick(),
        }),
        decorated: new GType.Property({
            type: GType.Bool,
            get: window => window.decorated
        }),
        remote: new GType.Property({
            type: GType.Bool,
            get: window => window.is_remote()
        })
    };

    static windowRunUnmaximizeIfNeeded(window, cb) {
        const sig =
        window.connect('size-changed', (window) => {
            window.disconnect(sig);
            const sig2 = window.connect('size-changed', (window) => {
                window.disconnect(sig2);
                cb(window);
            });
            if(window.maximized_vertically)
                window.unmaximize(Meta.MaximizeFlags.VERTICAL);
        });
        if (window.maximized_horizontally) 
            window.unmaximize(Meta.MaximizeFlags.HORIZONTAL);
    }

    static WindowLookupError = class extends Error {
        constructor(window_id) {
            super("Cannot find window with id " + window_id);
        } 
    }

    constructor(window) {
        const fields = Window.fields;
        for (const property in fields) {
            if (!(fields.reserved_properties.includes(property)))
                this[property] = fields[property].get(window);
        }
        this.getBackend = () => window;
    }

    pack() {
        const fields = Window.fields;
        return Object.fromEntries(
            Object.entries(this)
                .filter(entry => {
                    const property = entry[0];
                    return property in fields && 
                        !(fields.reserved_properties.includes(property));
                })
                .map(entry => {
                    const property = entry[0];
                    const value = entry[1];
                    return [property, fields[property].pack(value)];
                })
        );
    }

    static newWindow(window) { return new Window(window); }

}
