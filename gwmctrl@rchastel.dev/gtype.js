/* gtype.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import GLib from 'gi://GLib';

export class GType {

    static Error = class extends Error {
        constructor(msg) {
            super(msg);
        }
    }

    constructor(g_type, packer, unpacker) {
        this.repr = g_type;
        this.g_type = GLib.VariantType.new(g_type);
        this.pack = packer;
        this.unpack = (v) => {
            if (v == null)
                throw new GType.Error("not a variant");
            return unpacker(v);
        };
    }
}

export const Str = new GType(
    's',
    GLib.Variant.new_string,
    (v) => v.get_string()
);

export const Bool = new GType(
    'b',
    GLib.Variant.new_boolean,
    (v) => v.get_boolean()
);

export const Int = new GType(
    'x',
    GLib.Variant.new_int64,
    (v) => v.get_int64()
);

export function Dict(type, elements) {
    return new GType(
        type,
        (d) => new GLib.Variant(type, d),
        (v) => (() => {
            const d = GLib.VariantDict.new(v);
            let obj = {};
            for (let property in elements) {
                const e_type = elements[property];
                obj[property] =
                    e_type.unpack(d.lookup_value(property, e_type.g_type));
            }
            return obj;
        })()
    );
}

export class Property {
    constructor(spec) {
        this.type = spec.type;
        this.get = spec.get;
        this.pack = this.type.pack;
        this.unpack = this.type.unpack;
        if(spec.set != undefined) this.set = spec.set;
        for (const property in spec) {
            if(!(property in ['type', 'get', 'set'])){
                this[property] = spec[property];
            }
        }
    }
}
